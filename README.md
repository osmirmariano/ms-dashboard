# Gerenciador

Api para aplicação que faz gerenciamento de tarefas


# Usuários
-- Clientes - permissão: user.client
-- Gerente de Projetos - permissão: user.manager
-- Desenvolvedor - permissão: user.developer 
-- Administrador - master

```json
{
    // Use IntelliSense to learn about possible attributes.
    // Hover to view descriptions of existing attributes.
    // For more information, visit: https://go.microsoft.com/fwlink/?linkid=830387
    "version": "0.2.0",
    "configurations": [
      {
        "type": "node",
        "request": "launch",
        "name": "Dev Maqpay",
        "protocol": "inspector",
        "stopOnEntry": false,
        "args": [],
        "preLaunchTask": null,
        "runtimeExecutable": null,
        "outputCapture": "std",
        "runtimeArgs": [
            "--nolazy"
        ],
        "skipFiles": [
            "<node_internals>/**"
        ],
        "program": "${workspaceFolder}/app.js"
      }
    ]
}
```

sls deploy --stage dev