
"use strict";
const router = require("express").Router();

const instructionController = require("../../controllers/instruction/instruction");
const auth = require("../../middlewares/token");
const permission = require("../../middlewares/permission");

router.use(auth.check);
router.get('', instructionController.show);
router.use(permission.checkAllUsers);
router.post('', instructionController.store);
router.put('/:id', instructionController.update);
router.get('', instructionController.show);
router.delete('/:id', instructionController.delete);

module.exports = router;
