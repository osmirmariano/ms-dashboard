
"use strict";
const router = require("express").Router();
const multer = require('multer');
// const uploadConfig = require('../../../config/upload');
// const upload = multer(uploadConfig);

const projectController = require("../../controllers/project/project")
const auth = require("../../middlewares/token");
const permission = require("../../middlewares/permission");

router.get('', projectController.show);
router.use(auth.check);
router.get('/users/list', projectController.userProject);
router.get('/all/:id', projectController.showAll);
// router.get('/users', projectController.showUserProject);

router.use(permission.checkManager);
router.post('', projectController.store);
router.put('/:id', projectController.update);
router.get('/:id', projectController.showById);

router.delete('/:id', projectController.delete);
router.put('/share/project', projectController.share);
router.put('/start/:id', projectController.startProject);
router.put('/end/:id', projectController.endProject);
router.put('/cancel/:id', projectController.cancelProject);



module.exports = router;
