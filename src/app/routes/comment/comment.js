
"use strict";
const router = require("express").Router();

const commentController = require("../../controllers/comment/comment");
const auth = require("../../middlewares/token")

router.use(auth.check);
router.post('', commentController.store);
router.get('', commentController.show);
router.get('/count', commentController.count)

module.exports = router;
