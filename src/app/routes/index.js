
const router = require('express').Router();
const packageJson = require('../../../package.json');

router.get('/', (req, res) => {
    res.type('text/plain');
    res.send(`${packageJson.name}`);
})

router.use("/comment", require("./comment/comment"));
router.use("/instruction", require("./instruction/instruction"));
router.use("/note", require("./note/note"));
router.use("/project", require("./project/project"));
router.use("/tool", require("./tool/tool"));
router.use("/user", require("./user/user"));
router.use("/login", require("./user/login"));
router.use("/admin", require("./admin/admin"));

module.exports = router;