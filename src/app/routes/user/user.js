
"use strict";
const router = require("express").Router();
const multer = require('multer');
const uploadConfig = require('../../../config/upload');
const upload = multer(uploadConfig);

const userController = require("../../controllers/user/user");
const auth = require("../../middlewares/token");

router.post("", upload.single('photo'),  userController.store);
router.use(auth.check);
router.put("/:id", userController.update);
router.get("/:id", userController.showById);
router.get("/developer/list", userController.showDeveloper);
router.get("/manager/list", userController.showManager);
router.get("/design/list", userController.showDesign);
router.get("/client/list", userController.showClient);

module.exports = router;
