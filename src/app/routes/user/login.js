
"use strict";
const router = require("express").Router();
const loginController = require("../../controllers/user/login")
const auth = require("../../middlewares/token");

router.post('', loginController.login);
router.use(auth.check);
router.get('/checkToken', loginController.checkToken);

module.exports = router;
