
"use strict";
const router = require("express").Router();

const noteController = require("../../controllers/note/note");
const auth = require("../../middlewares/token");

router.use(auth.check);
router.post('', noteController.store);
router.put('/:id', noteController.update);
router.delete('/:id', noteController.delete);
router.get('', noteController.show);
router.put('/share/:id', noteController.share);
router.get('/:id', noteController.noteById);


module.exports = router;
