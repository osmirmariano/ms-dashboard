
"use strict";
const router = require("express").Router();
const toolController = require("../../controllers/tool/tool")
const auth = require("../../middlewares/token");
const permission = require("../../middlewares/permission");

router.use(auth.check);
router.get('', toolController.show);
router.use(permission.checkAllUsers);
router.post('', toolController.store);
router.put('/:id', toolController.update);
router.delete('/:id', toolController.delete)

module.exports = router;
