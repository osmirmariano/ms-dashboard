
"use strict";
const router = require("express").Router();
const adminUserController = require("../../controllers/admin/user/user");
const adminProjectController = require("../../controllers/admin/project/project");
const admin = require("../../middlewares/admin");

router.get("/user", adminUserController.show);
router.use(admin.checkAdmin);
router.put("/user/:id", adminUserController.blocked);
router.delete("/user/:id", adminUserController.delete);

router.get("/project", adminProjectController.show);


module.exports = router;
