//database configuration module comment
const mongoose = require('../../../config/database');
//define the schema of comment
const commentSchema = mongoose.Schema({
    project: {
        id_project: { 
            type: mongoose.Schema.Types.ObjectId, ref: 'project', 
            default: null,
            required: true
        },
    },
    user: {
        id_user: { 
            type: mongoose.Schema.Types.ObjectId, ref: 'user', 
            default: null,
            required: true
        },
    },
    instruction: {
        id_instruction: { 
            type: mongoose.Schema.Types.ObjectId, ref: 'instruction', 
            default: null,
            required: true
        },
    },
    text: {
        type: String,
        required: true
    },
    registered: { type: Date, default: Date.now },
    updated: { type: Date, default: null },
});
//define the database comment model with all fields
const comment = mongoose.model('comment', commentSchema);
//exports the module, in which case it must be required if you want to manipulate this collection
module.exports = comment;
        