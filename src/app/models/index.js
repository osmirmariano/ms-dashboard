
module.exports = {
    users: require('./user/user'),
    comments: require('./comment/comment'),
    instructions: require('./instruction/instruction'),
    notes: require('./note/note'),
    projects: require('./project/project'),
    tools: require('./tool/tool')
}
