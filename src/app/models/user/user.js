//database configuration module user
const mongoose = require('../../../config/database');
//define the schema of user
const userSchema = mongoose.Schema({
    user: { 
        type: String, 
        lowercase: true, 
        maxlength: 40, 
        unique: true, 
        sparse: true, 
        required: [true, 'Usuário não informado.'] 
    },
    name: { 
        type: String, 
        maxlength: 120, 
        required: [true, 'Nome não informado.'] 
    },
    password: {
        password: { type: String, required: [true, 'Senha não informada.'] },
        requested_change: { type: Boolean, default: false },
        last_change: { type: Date, default: null },
    },
    photo: {
        type: String,
    },
    email: {
        email: {
            type: String, 
            lowercase: true, 
            maxlength: 200, 
            required: [true, 'Email não informado.'], 
            unique: true, 
            sparse: true
        },
        email_change: {
            type: String, 
            lowercase: true, 
            maxlength: 200
        }
    },
    permissions: [{
        type: String
    }],
    phone: {
        type: String, maxlength: 30, lowercase: true, sparse: true
    },
    blocked: {
        type: Boolean,
        default: false
    },
    profile: {
        type: String,
        default: null
    },
    able: {
        type: Boolean,
        default: false
    },
    color: {
        type: String,
        enum: { 
            values: ['#0075ff', '#34A031', '#e99f01', '#c0c0c0'] 
        },
    },
    registered: { type: Date, default: Date.now },
    updated: { type: Date, default: null },
});
//define the database user model with all fields
const user = mongoose.model('user', userSchema);
//exports the module, in which case it must be required if you want to manipulate this collection
module.exports = user;
        