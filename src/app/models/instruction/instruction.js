//database configuration module instruction
const mongoose = require('../../../config/database');
//define the schema of instruction
const instructionSchema = mongoose.Schema({
    project: {
        id_project: { 
            type: mongoose.Schema.Types.ObjectId, ref: 'project', 
            default: null,
            required: true
        },
    },
    user: {
        id_user: { 
            type: mongoose.Schema.Types.ObjectId, ref: 'user', 
            default: null,
            required: true
        },
    },
    title: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    registered: { type: Date, default: Date.now },
    updated: { type: Date, default: null },
});
//define the database instruction model with all fields
const instruction = mongoose.model('instruction', instructionSchema);
//exports the module, in which case it must be required if you want to manipulate this collection
module.exports = instruction;
        