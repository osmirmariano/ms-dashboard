//database configuration module project
const mongoose = require('../../../config/database');
const { PERMISSIONS } = require('../../constants/constants');
//define the schema of project
const projectSchema = mongoose.Schema({
    title: { 
        type: String,
        required: [true, 'Titulo não informado.'] 
    },
    description: { 
        type: String,
        required: [true, 'Descrição não informado.'] 
    },
    status: {
        type: String,
        enum: { values: ['create', 'running', 'finalized', 'canceled', 'paused'] },
    },
    category: [{
        type: String,
        required: [true, 'Categoria não informado.'] 
    }],
    photo_project: {
        type: String,
    },
    label: {
        type: String,
        enum: { values: ['blue', 'yellow', 'green', 'red', 'gray', 'orange', 'purple', 'brown'] },
        default: 'blue'
    },
    user: {
        id_user: {
            type: mongoose.Schema.Types.ObjectId, ref: 'user', 
            default: null 
        },
        id_user_sharing: [{ type: mongoose.Schema.Types.ObjectId, ref: 'user' }],
        permissions: [{
            type: String, enum: PERMISSIONS
        }],
    },
    tool: {
        id_tool: {
            type: mongoose.Schema.Types.ObjectId, ref: 'tool', 
            default: null 
        },
    },
    date_start: {
        type: Date,
    },
    date_end: {
        type: Date
    },
    registered: { type: Date, default: Date.now },
    updated: { type: Date, default: null },
});
//define the database project model with all fields
const project = mongoose.model('project', projectSchema);
//exports the module, in which case it must be required if you want to manipulate this collection
module.exports = project;
        