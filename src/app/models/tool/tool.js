//database configuration module tool
const mongoose = require('../../../config/database');
//define the schema of tool
const toolSchema = mongoose.Schema({
    project: {
        id_project: { 
            type: mongoose.Schema.Types.ObjectId, ref: 'project', 
            default: null,
            required: true
        },
    },
    user: {
        id_user: { 
            type: mongoose.Schema.Types.ObjectId, ref: 'user', 
            default: null,
            required: true
        },
    },
    url: {
        type: String,
        default: null
    },
    label: {
        type: String,
        default: null
    },
    category: {
        type: String,
        default: null
    },
    title: {
        type: String,
        default: null
    },
    registered: { type: Date, default: Date.now },
    updated: { type: Date, default: null },
});
//define the database tool model with all fields
const tool = mongoose.model('tool', toolSchema);
//exports the module, in which case it must be required if you want to manipulate this collection
module.exports = tool;
        