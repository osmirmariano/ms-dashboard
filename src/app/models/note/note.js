//database configuration module note
const mongoose = require('../../../config/database');
//define the schema of note
const noteSchema = mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    start_date: {
        type: Date, 
        default: null
    },
    end_date: {
        type: Date, 
        default: null
    },
    project: {
        id_project: { 
            type: mongoose.Schema.Types.ObjectId, ref: 'project', 
            default: null,
            required: true
        },
    },
    user: {
        id_user: { 
            type: mongoose.Schema.Types.ObjectId, ref: 'user', 
            default: null,
            required: true
        },
        id_user_sharing: {
            type: mongoose.Schema.Types.ObjectId, ref: 'user'
        }
    },
    priority: {
        type: String,
        enum: { values: ['gentle', 'middle', 'urgent', 'extreme'] },
        default: "gentle"
    },
    registered: { type: Date, default: Date.now },
    updated: { type: Date, default: null },
});
//define the database note model with all fields
const note = mongoose.model('note', noteSchema);
//exports the module, in which case it must be required if you want to manipulate this collection
module.exports = note;
        