const PERMISSIONS = [
    "master",
    "user.client",
    "user.manager",
    "user.developer",
    "user.design",
    "list.user.all"
];

module.exports = {
    PERMISSIONS
}