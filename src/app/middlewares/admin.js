const keys = require('../constants/keys');

class Admin { 
    constructor() {}

    /**
     * Method for check profile admin
     * @param {*} req 
     * @param {*} res 
     * @param {*} next 
     */
    async checkAdmin(req, res, next) {
        const key = req.headers.key;
        try {
            let user = await keys.find(x => x == key);
            user ? 
                next()
            : res.status(401).json({
                messageCode: 3,
                message: {
                    title: "Erro",
                    message: "Você não permissão de administrador"
                }
            })
        } catch (error) {
            res.status(301).json({
                messageCode: 5,
                message: {
                    title: "Erro",
                    message: "Não foi possível processar a requisição!"
                }
            })   
        }
    }
} 

module.exports = new Admin();
