const { tools, projects, users } = require('../../models/index');
var jwt = require('jsonwebtoken');

class Tool { 
    constructor() {}

    /**
     * Create project
     * @param {*} req 
     * @param {*} res 
     */
    async store (req, res) {
        try {
            let projectFineld = await projects.findById({ _id: req.query.id_project }).exec();
            if(!projectFineld) 
                res.status(400).json({
                    messageCode: 3,
                    message: {
                        title: "Erro",
                        message: "Não foi adicionar repositórios, pois o id do projeto não é válido"
                    },
                    erro: error
                })

            let toolFineld = {
                ...req.body,
                'user.id_user': jwt.decode(req.headers.authorization).id,
                'project.id_project': req.query.id_project
            }

            let toolFind = await tools.create(toolFineld)
            res.status(200).json({
                messageCode: 0,
                message: {
                    title: "Sucesso",
                    message: "Projeto repositório adicionado com sucesso!"
                },
                data: toolFind
            })
        } catch (error) {
            res.status(400).json({
                messageCode: 3,
                message: {
                    title: "Erro",
                    message: "Não foi possível adicionar repositório projeto"
                },
                erro: error
            })
        }
    }

    /**
     * List all tools
     * @param {*} req 
     * @param {*} res 
     */
    async show (req, res) {
        try {
            let toolFind = await tools.find({ 'project.id_project': req.query.id_project }).exec();
            res.status(200).json({
                messageCode: 0,
                message: {
                    title: "Sucesso",
                    message: "Projeto repositório adicionado com sucesso!"
                },
                data: toolFind
            })
        } catch (error) {
            res.status(400).json({
                messageCode: 3,
                message: {
                    title: "Erro",
                    message: "Não foi possível adicionar repositório projeto"
                },
                erro: error
            })
        }
    }

    /**
     * Update tools
     * @param {*} req 
     * @param {*} res 
     */
    async update (req, res) {
        try {
            let projectFineld = await projects.findById({ _id: req.query.id_project }).exec();
            if(!projectFineld) 
                res.status(400).json({
                    messageCode: 3,
                    message: {
                        title: "Erro",
                        message: "Não foi atualizar instrução, pois o id do projeto não é válido"
                    },
                    erro: error
                })

            let toolFind = await tools.findByIdAndUpdate({ _id: req.params.id }, req.body, { new: true}).exec();
            res.status(200).json({
                messageCode: 0,
                message: {
                    title: "Sucesso",
                    message: "Projeto repositório atualizado com sucesso!"
                },
                data: toolFind
            })
        } catch (error) {
            res.status(400).json({
                messageCode: 3,
                message: {
                    title: "Erro",
                    message: "Não foi possível atualizar repositório projeto"
                },
                erro: error
            })
        }
    }

    /**
     * Delete tool
     * @param {*} req 
     * @param {*} res 
     */
    async delete (req, res) {
        try {
            await tools.findByIdAndDelete({ _id: req.params.id })
            res.status(200).json({
                messageCode: 0,
                message: {
                    title: "Sucesso",
                    message: "Ferramenta deletado com sucesso!"
                },
            })
        } catch (error) {
            res.status(400).json({
                messageCode: 3,
                message: {
                    title: "Erro",
                    message: "Não foi possível deletar Ferramenta do projeto"
                },
                erro: error
            })
        }
    }
} 

module.exports = new Tool();
