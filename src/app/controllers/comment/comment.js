const { comments, projects } = require('../../models/index');
const mongoose = require('mongoose')
const objectId = mongoose.Types.ObjectId

class Comment { 
    constructor() {}

    /**
     * Create comment
     * @param {*} req 
     * @param {*} res 
     */
    async store (req, res) {
        try {
            let projectComment = await projects.findOne({ _id: req.query.id_project })
            if(!projectComment) {
                res.status(400).json({
                    messageCode: 3,
                    message: {
                        title: "Erro",
                        message: "Não foi possível enviar comentário, pois id do projeto não foi encontrado"
                    },
                    err: err
                })
            }
            let commentFineld = {
                ...req.body,
                "user.id_user": req.query.id_user,
                "project.id_project": req.query.id_project,
                "instruction.id_instruction": req.query.id_instruction
            }

            let comment = await comments.create(commentFineld);
            res.status(200).json({
                messageCode: 0,
                message: {
                    title: "Sucesso",
                    message: "Comentário enviado com sucesso!"
                },
                data: comment
            })
        } catch (error) {
            res.status(400).json({
                messageCode: 3,
                message: {
                    title: "Erro",
                    message: "Não foi possível enviar comentário"
                },
                err: error
            })
        }
    }

    /**
     * View comment by id instruction
     * @param {*} req 
     * @param {*} res 
     */
    async show (req, res) {
        try {
            let page = 1
            let results = 100
            let queryGeneric = {
                "instruction.id_instruction": new objectId(req.query.id_instruction)
            }

            if (req.query.page)
                page = parseInt(req.query.page)
            if (req.query.results)
                results = parseInt(req.query.results)
            
            let count = await comments.countDocuments(queryGeneric);
            let comentarios = await comments.aggregate([
                {
                    $match: {
                        "instruction.id_instruction": new objectId(req.query.id_instruction)
                    }, 
                }, {
                    $sort: {
                        _id: -1
                    }
                },
                {
                    $lookup: {
                        from: "users",
                        let: { 
                            id: "$user.id_user"
                        },
                        pipeline: [
                            {
                                $match: {
                                    $expr: {
                                        $and: [
                                            { $eq: ['$_id', '$$id'] }
                                        ]
                                    }
                                }
                            }
                        ],
                        as: "user"
                    },
                }, {
                    $unwind: {
                        path: "$user"
                    }
                }, { 
                    $skip : (page * results) - results
                }, { 
                    $limit : results
                }, { 
                    $sort: { registered: -1 }
                }
            ])
            .exec()


            res.send({
                messageCode: 0,
                count: count,
                pageNumbers: Math.ceil((count / results)),
                page: page,
                list: comentarios,
                message: {
                    title: 'Sucesso',
                    message: 'Comentários listados com sucesso!'
                }
            })
        } catch (error) {
            res.status(400).json({
                messageCode: 3,
                message: {
                    title: "Erro",
                    message: "Não foi possível enviar comentário"
                },
                err: error
            })
        }
    }

    async count(id_project) {
        try {
            let count = await comments.find({ "project.id_project": id_project }).count();
            res.status(200).json({
                messageCode: 0,
                message: {
                    title: "Sucesso",
                    message: "Quantidade de comentários listados com sucesso!"
                },
                data: count
            })
        } catch (error) {
            res.status(400).json({
                messageCode: 3,
                message: {
                    title: "Erro",
                    message: "Não foi possível listar quantidade de comentários"
                },
                err: error
            })
        }
    }
} 

module.exports = new Comment();
