const { notes, projects } = require('../../models/index')
var moment = require('moment');
var jwt = require('jsonwebtoken');
const mongoose = require('mongoose')
const objectId = mongoose.Types.ObjectId

class Note { 
    constructor() {}

    /**
     * Create note
     * @param {*} req 
     * @param {*} res 
     */
    async store (req, res) {
        let projectFind = await projects.findById({_id: req.query.id_project })

        if(!projectFind)
            res.status(400).json({
                messageCode: 3,
                message: {
                    title: "Erro",
                    message: "Id do projeto não foi encontrado"
                }
            })
        else {
            req.body.start_date = moment(req.body.start_date).format("YYYY-MM-DDTHH:mm:ss");
            req.body.end_date = moment(req.body.end_date).format("YYYY-MM-DDTHH:mm:ss");

            let noteFineld = {
                ...req.body,
                "project.id_project": req.query.id_project,
                "user.id_user": jwt.decode(req.headers.authorization).id
            }

            try {
                let note = await notes.create(noteFineld)
                res.status(200).json({
                    messageCode: 0,
                    message: {
                        title: "Sucesso",
                        message: "Nota criada com sucesso!"
                    },
                    data: note
                })
            } catch (error) {
                res.status(400).json({
                    messageCode: 3,
                    message: {
                        title: "Erro",
                        message: "Não foi possível criar card"
                    },
                    err: error
                })
            }
        }
    }

    /**
     * Update note
     * @param {*} req 
     * @param {*} res 
     */
    async update (req, res) {
        try {
            let noteCreate = await notes.findOneAndUpdate({ _id: req.params.id },  req.body, { new: true });
            res.status(200).json({
                messageCode: 0,
                message: {
                    title: "Sucesso",
                    message: "Nota atualizado com sucesso!"
                },
                data: noteCreate
            })
        } catch (error) {
            res.status(301).json({
                messageCode: 3,
                message: {
                    title: "Erro",
                    message: "Não foi possível editar a nota"
                }
            })
        }
    }

    /**
     * Delete notes
     * @param {*} req 
     * @param {*} res 
     */
    async delete (req, res) {
        try {
            await notes.findOneAndDelete({ _id: req.params.id });
            res.status(200).json({
                messageCode: 0,
                message: {
                    title: "Sucesso",
                    message: "Nota excluida com sucesso!"
                },
            })
        } catch (error) {
            res.status(301).json({
                messageCode: 3,
                message: {
                    title: "Erro",
                    message: "Não foi possível excluir nota"
                }
            })
        }
    }

    /**
     * List instruction by id project
     * @param {*} req 
     * @param {*} res 
     */
    async show (req, res) {
        try {
            // let listNotes = await notes.find({ 'project.id_project': req.query.id_project });

            let listNotes = await notes.aggregate([
                {
                    $match: {
                        "project.id_project": new objectId(req.query.id_project)
                    }, 
                }, {
                    $sort: {
                        _id: -1
                    }
                },
                {
                    $lookup: {
                        from: "users",
                        let: { 
                            id: "$user.id_user_sharing"
                        },
                        pipeline: [
                            {
                                $match: {
                                    $expr: {
                                        $and: [
                                            { $eq: ['$_id', '$$id'] }
                                        ]
                                    }
                                }
                            }
                        ],
                        as: "user_sharing"
                    },
                }, {
                    $unwind: {
                        path: "$user"
                    }
                }
            ]).exec();



            res.status(200).json({
                messageCode: 0,
                message: {
                    title: "Sucesso",
                    message: "Notas listadas com sucesso"
                },
                data: listNotes
            })
        } catch (error) {
            res.status(400).json({
                messageCode: 3,
                message: {
                    title: "Erro",
                    message: "Não foi possível listar as notas"
                },
                error: error
            })
        }
    }

    async share(req, res) {
        try {
            await notes.findOneAndUpdate({ _id: req.params.id }, { 
                'user.id_user_sharing': req.query.id_user
            }, { new: true }).exec();
            
            res.status(200).json({
                messageCode: 0,
                message: {
                    title: "Sucesso",
                    message: "Usuário atribuido a nota com sucesso"
                }
            })
        } catch (error) {
            res.status(400).json({
                messageCode: 3,
                message: {
                    title: "Erro",
                    message: "Não foi possível atribuir essa nota"
                },
                error: error
            })
        }
    }

    async noteById(req, res) {
        try {
            // let note = await notes.findById({ _id: req.params.id });
            let note = await notes.aggregate([
                {
                    $match: {
                        "_id": new objectId(req.params.id)
                    }, 
                }, {
                    $sort: {
                        _id: -1
                    }
                },
                {
                    $lookup: {
                        from: "users",
                        let: { 
                            id: "$user.id_user_sharing"
                        },
                        pipeline: [
                            {
                                $match: {
                                    $expr: {
                                        $and: [
                                            { $eq: ['$_id', '$$id'] }
                                        ]
                                    }
                                }
                            }
                        ],
                        as: "user_sharing"
                    },
                }, {
                    $unwind: {
                        path: "$user"
                    }
                }
            ]).exec();



            res.status(200).json({
                messageCode: 0,
                message: {
                    title: "Sucesso",
                    message: "Nota listada com sucesso"
                },
                data: note[0]
            })
        } catch (error) {
            res.status(400).json({
                messageCode: 3,
                message: {
                    title: "Erro",
                    message: "Não foi possível listar nota"
                },
                error: error
            })
        }
    }
} 

module.exports = new Note();
