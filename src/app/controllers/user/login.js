const { users } = require('../../models/index')
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var config = require('../../../config/config');

class Login {
    /**
     * Method for login in users
     * @param {*} req 
     * @param {*} res 
     */
    async login (req, res) {
        try {
            var userFinded = await users.findOne({ user: req.body.user }).exec();
            if(!userFinded) {
                res.status(400).json({
                    messageCode: 3,
                    message: {
                        title: "Erro",
                        message: "Usuário não encontrado"
                    }
                })
            } else {
                if(userFinded.blocked == true)
                    return res.status(400).json({
                        messageCode: 5,
                        message: {
                            title: "Erro",
                            message: "Usuário bloqueado por infringir as políticas."
                        }
                    })

                var passwordIsValid = bcrypt.compareSync(req.body.password, userFinded.password.password);
                
                if (!passwordIsValid) {
                    var auth = {
                        auth: false, 
                        type: null,
                        token: null
                    }
                    
                    res.status(400).json({
                        messageCode: 3,
                        message: {
                            title: "Erro",
                            message: "Usuário e/ou senha não encontrado"
                        },
                        auth: auth
                    })
                }
                
                var token = jwt.sign({ id: userFinded._id }, config.secret, {
                    expiresIn: 86400 // expires in 24 hours
                });
        
                var auth = {
                    auth: true, 
                    type: "Bearer",
                    token: token
                }
                
                res.status(200).json({
                    messageCode: 0,
                    message: {
                        title: "Sucesso",
                        message: "Login realizado com sucesso!"
                    },
                    data: {
                        ...auth,
                        user: userFinded.user,
                        _id: userFinded._id
                    }
                })
            }
        } catch (error) {
            res.status(400).json({
                messageCode: 3,
                message: {
                    title: "Erro",
                    message: "Usuário não encontrado"
                }
            })
        }
    }

    async checkToken(req, res) {
        var tokenDecode = jwt.decode(req.headers.authorization);

        const user = await users.findOne({ _id: tokenDecode.id }).exec();
        res.status(200).json({
            token:  req.headers.authorization,
            user: user.user,
            name: user.name,
            _id: user._id
        })
    }
}

module.exports = new Login();