const { users } = require('../../models/index');
var bcrypt = require('bcryptjs');

class User { 
    constructor() {}

    /**
     * Create user account
     * @param {*} req 
     * @param {*} res 
     */
    async store (req, res) {
        let hash = bcrypt.hashSync(req.body.password, 8);
        const { filename } = req.file;
        let profile;
        let color;
        if(req.body.profile == "client") {
            profile = "user.client";
            color = "#c0c0c0";
        } else if(req.body.profile == "manager") {
            profile = "user.manager";
            color = "#e99f01";
        } else if(req.body.profile == "developer") {
            profile = "user.developer";
            color = "#34A031";
        } else if(req.body.profile == "design") {
            profile = "user.design";
            color = "#0075ff";
        }

        if(profile == null) {
            res.status(400).json({
                messageCode: 3,
                message: {
                    title: "Erro",
                    message: "Não foi possível criar usuário"
                },
                erro: {
                    erro: "Tipo de conta não informado"
                }
            })
        } else {
            let userFineld = {
                ...req.body,
                photo: filename,
                "password.password": hash,
                "email.email": req.body.email,
                "permissions": profile,
                "color": color
            }
    
            try {
                let userFined = await users.create(userFineld)
                res.status(200).json({
                    messageCode: 0,
                    message: {
                        title: "Sucesso",
                        message: "Usuário registrado com sucesso!"
                    },
                    data: userFined
                })
            } catch (err) {
                res.status(400).json({
                    messageCode: 3,
                    message: {
                        title: "Erro",
                        message: "Não foi possível criar usuário"
                    },
                    erro: err
                })
            }
        }

    }

    /**
     * Update user
     * @param {*} req 
     * @param {*} res 
     */
    async update (req, res) {
        try {
            //const { filename } = req.file;
            let body = {
                ...req.body,
                //photo: filename
            }
            let userUpdate = await users.findByIdAndUpdate({ _id: req.params.id }, body, { new: true }).exec();
            
            userUpdate 
            ?
                res.status(200).json({
                    messageCode: 0,
                    message: {
                        title: "Sucesso",
                        message: "Usuário atualizado com sucesso!"
                    },
                    data: userUpdate
                })
            :
                res.status(400).json({
                    messageCode: 3,
                    message: {
                        title: "Erro",
                        message: "Não foi possível encontrar usuário com esse ID"
                    },
                    error: {}
                })
        } catch (error) {
            res.status(301).json({
                messageCode: 3,
                message: {
                    title: "Erro",
                    message: "Não foi possível editar usuário"
                },
                error: error
            })
        }
    }

    /**
     * List all user with profile in developer
     * @param {*} req 
     * @param {*} res 
     */
    async showById (req, res) {
        try {
            let userInfo = await users.findOne({ _id: req.params.id }, '-password').exec();
            res.status(200).json({
                messageCode: 0,
                message: {
                    title: "Sucesso",
                    message: "Usuário listado com sucesso!"
                },
                data: userInfo
            })
        } catch (error) {
            res.status(400).json({
                messageCode: 3,
                message: {
                    title: "Erro",
                    message: "Erro no banco de dados"
                },
                erro: error
            })
        }
    }

    /**
     * List all user with profile in developer
     * @param {*} req 
     * @param {*} res 
     */
    async showDeveloper(req, res) {
        try {
            let usersDeveloper = await users.find({ profile: "developer" }, '-password').exec();
            res.status(200).json({
                messageCode: 0,
                message: {
                    title: "Sucesso",
                    message: "Usuário listados com sucesso!"
                },
                data: usersDeveloper
            })
        } catch (error) {
            console.log(error)
            res.status(400).json({
                messageCode: 3,
                message: {
                    title: "Erro",
                    message: "Erro no banco de dados"
                },
                erro: error
            })
        }
    }

    /**
     * List all user with profile in manager
     * @param {*} req 
     * @param {*} res 
     */
    async showManager (req, res) {
        try {
            let usersManager = await users.find({ profile: "manager" }, '-password').exec();
            res.status(200).json({
                messageCode: 0,
                message: {
                    title: "Sucesso",
                    message: "Usuário listados com sucesso!"
                },
                data: usersManager
            })
        } catch (error) {
            res.status(400).json({
                messageCode: 3,
                message: {
                    title: "Erro",
                    message: "Erro no banco de dados"
                },
                erro: error
            })
        }
    }

    /**
     * List all user with profile in design
     * @param {*} req 
     * @param {*} res 
     */
    async showDesign (req, res) {
        try {
            let usersDesign = await users.find({ profile: "design" }, '-password').exec();
            res.status(200).json({
                messageCode: 0,
                message: {
                    title: "Sucesso",
                    message: "Usuário listados com sucesso!"
                },
                data: usersDesign
            })
        } catch (error) {
            res.status(400).json({
                messageCode: 3,
                message: {
                    title: "Erro",
                    message: "Erro no banco de dados"
                },
                erro: error
            })
        }
    }

    /**
     * List all user with profile in client
     * @param {*} req 
     * @param {*} res 
     */
    async showClient (req, res) {
        try {
            let usersDesign = await users.find({ profile: "client" }, '-password').exec();
            res.status(200).json({
                messageCode: 0,
                message: {
                    title: "Sucesso",
                    message: "Usuário listados com sucesso!"
                },
                data: usersDesign
            })
        } catch (error) {
            res.status(400).json({
                messageCode: 3,
                message: {
                    title: "Erro",
                    message: "Erro no banco de dados"
                },
                erro: error
            })
        }
    }

    /**
     * Search users
     * @param {*} req 
     * @param {*} res 
     */
    async searchUser (req, res) {
        console.log('kkk')
        await user.createIndexes({
            user: "text"
        })

        let u = await users.find(
            { '$text': {'$search' : 'osmirmariano' } }
        ).exec();
        console.log(u);
    }
} 

module.exports = new User();
