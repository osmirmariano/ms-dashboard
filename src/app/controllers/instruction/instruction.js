const { instructions, projects } = require('../../models/index');
var jwt = require('jsonwebtoken');
const mongoose = require('mongoose');
const objectId = mongoose.Types.ObjectId;

class Instruction { 
    constructor() {}

    /**
     * Create instructions
     * @param {*} req 
     * @param {*} res 
     */
    async store (req, res) {
        try {
            let projectFineld = await projects.findById({ _id: req.query.id_project }).exec();
            if(!projectFineld) 
                res.status(400).json({
                    messageCode: 3,
                    message: {
                        title: "Erro",
                        message: "Não foi criar instrução, pois o id do projeto não é válido"
                    },
                    erro: error
                })

            let insctructionFineld = {
                ...req.body,
                'user.id_user': jwt.decode(req.headers.authorization).id,
                'project.id_project': projectFineld._id
            }
            let createInstruction = await instructions.create(insctructionFineld)
            res.status(200).json({
                messageCode: 0,
                message: {
                    title: "Sucesso",
                    message: "Instrução criada com sucesso!"
                },
                data: createInstruction
            })
        } catch (error) {
            res.status(400).json({
                messageCode: 3,
                message: {
                    title: "Erro",
                    message: "Não foi possível criar instrução"
                },
                erro: error
            })
        }
    }

    /**
     * Update instructions
     * @param {*} req 
     * @param {*} res 
     */
    async update (req, res) {
        try {
            let projectFineld = await projects.findById({ _id: req.query.id_project }).exec();
            if(!projectFineld) 
                res.status(400).json({
                    messageCode: 3,
                    message: {
                        title: "Erro",
                        message: "Não foi atualizar instrução, pois o id do projeto não é válido"
                    },
                    erro: error
                })
                
                let instructionsUpdate = await instructions.findByIdAndUpdate({ _id: req.params.id }, req.body, { new: true}).exec();
                res.status(200).json({
                    messageCode: 0,
                    message: {
                        title: "Sucesso",
                        message: "Instrução criada com sucesso!"
                    },
                    data: instructionsUpdate
                })
        } catch (error) {
            res.status(400).json({
                messageCode: 3,
                message: {
                    title: "Erro",
                    message: "Não foi possível atualizar instrução"
                },
                erro: error
            })
        }
    }

    /**
     * Delete instructions
     * @param {*} req 
     * @param {*} res 
     */
    async delete (req, res) {
        try {
            let projectFineld = await projects.findById({ _id: req.query.id_project }).exec();
            if(!projectFineld) 
                res.status(400).json({
                    messageCode: 3,
                    message: {
                        title: "Erro",
                        message: "Não foi atualizar instrução, pois o id do projeto não é válido"
                    },
                    erro: error
                })

                await instructions.findOneAndDelete({ _id: req.params.id })
                res.status(200).json({
                    messageCode: 0,
                    message: {
                        title: "Sucesso",
                        message: "Instrução deletada com sucesso!"
                    },
                })
        } catch (error) {
            res.status(400).json({
                messageCode: 3,
                message: {
                    title: "Erro",
                    message: "Não foi possível excluir instrução"
                },
                erro: error
            })
        }
    }

    /**
     * List instructions by id projects
     * @param {*} req 
     * @param {*} res 
     */
    async show (req, res) {
        try {
            let projectFineld = await projects.findById({ _id: req.query.id_project }).exec();
            if(!projectFineld) 
                res.status(400).json({
                    messageCode: 3,
                    message: {
                        title: "Erro",
                        message: "Não foi listar instrução, pois o id do projeto não é válido"
                    },
                    erro: error
                })

            let instructionsList = await instructions.aggregate([
                {
                    $match: {
                        "project.id_project": new objectId(req.query.id_project)
                    }, 
                }, {
                    $sort: {
                        _id: -1
                    }
                },
                {
                    $lookup: {
                        from: "users",
                        let: { 
                            id: "$user.id_user"
                        },
                        pipeline: [
                            {
                                $match: {
                                    $expr: {
                                        $and: [
                                            { $eq: ['$_id', '$$id'] }
                                        ]
                                    }
                                }
                            }
                        ],
                        as: "user"
                    },
                }, {
                    $unwind: {
                        path: "$user"
                    }
                }
            ]).exec();

            res.status(200).json({
                messageCode: 0,
                message: {
                    title: "Sucesso",
                    message: "Instruções listadas com sucesso!"
                },
                data: instructionsList
            })
        } catch (error) {
            res.status(400).json({
                messageCode: 3,
                message: {
                    title: "Erro",
                    message: "Não foi possível listar as instruções"
                },
                erro: error
            })
        }
    }
} 

module.exports = new Instruction();
