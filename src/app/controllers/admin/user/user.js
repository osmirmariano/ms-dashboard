const { users } = require('../../../models/index');

class AdminUser { 
    constructor() {}

    /**
     * List all users, only users admin seach
     * @param {*} req 
     * @param {*} res 
     */
    async show (req, res) {
        let page = 1
        let results = 100
        let queryGeneric = {}

        if (req.query.page)
            page = parseInt(req.query.page)
        if (req.query.results)
            results = parseInt(req.query.results)

        users.countDocuments(queryGeneric, (err, count) => {
            users.find(queryGeneric, "-password", (err, finded) => {
                res.send({
                    messageCode: 0,
                    count: count,
                    pageNumbers: Math.ceil((count / results)),
                    page: page,
                    list: finded,
                    message: {
                        title: 'Sucesso',
                        message: 'Usuários listados com sucesso!'
                    }
                })
            })
            .limit(results)
            .skip((page * results) - results)
            .exec()
        })
    }

    /**
     * Delete user
     * @param {*} req 
     * @param {*} res 
     */
    async delete(req, res) {
        try {
            await users.findOneAndDelete({ _id: req.params.id }).exec();
            res.status(200).json({
                messageCode: 0,
                message: {
                    title: "Sucesso",
                    message: "Usuário deletado com sucesso!"
                },
                data: usersDesign
            })
        } catch (error) {
            res.status(301).json({
                messageCode: 3,
                message: {
                    title: "Erro",
                    message: "Não foi possível deletar usuário!"
                }
            })
        }
    }

    /**
     * Method for blocked user
     * @param {*} req 
     * @param {*} res 
     */
    async blocked(req, res) {
        try {
            let blockedControl;
            let userBlocked = await users.findOne({ _id: req.params.id }).exec();
            if(userBlocked) {
                userBlocked.blocked == true ? blockedControl = false : blockedControl = true
            } else {
                res.status(400).json({
                    messageCode: 3,
                    message: {
                        title: "Erro",
                        message: "Não foi encontrado nenhum usuário com esse id!"
                    }
                })
            }
            
            let user = await users.findOneAndUpdate({ _id: req.params.id }, { "blocked": blockedControl }, { new: true }).exec();
            res.status(200).json({
                messageCode: 0,
                message: {
                    title: "Sucesso",
                    message: "Usuário bloqueado com sucesso!"
                },
                data: user
            })
        } catch (error) {
            res.status(301).json({
                messageCode: 3,
                message: {
                    title: "Erro",
                    message: "Não foi possível bloquear usuário!"
                }
            })
        }
    }
} 

module.exports = new AdminUser();
