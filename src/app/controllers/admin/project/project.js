const { projects } = require('../../../models/index');

class AdminProject { 
    constructor() {}

    /**
     * List all users, only users admin seach
     * @param {*} req 
     * @param {*} res 
     */
    async show (req, res) {
        let page = 1
        let results = 100
        let queryGeneric = {}

        if (req.query.page)
            page = parseInt(req.query.page)
        if (req.query.results)
            results = parseInt(req.query.results)

        projects.countDocuments(queryGeneric, (err, count) => {
            projects.find(queryGeneric, (err, finded) => {
                res.send({
                    messageCode: 0,
                    count: count,
                    pageNumbers: Math.ceil((count / results)),
                    page: page,
                    list: finded,
                    message: {
                        title: 'Sucesso',
                        message: 'Projetos listados com sucesso!'
                    }
                })
            })
            .limit(results)
            .skip((page * results) - results)
            .exec()
        })
    }
} 

module.exports = new AdminProject();
