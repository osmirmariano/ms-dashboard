const { projects, users } = require('../../models/index');
var moment = require('moment');
var jwt = require('jsonwebtoken');
const mongoose = require('mongoose');
const objectId = mongoose.Types.ObjectId;

class Project { 
    constructor() {}

    /**
     * Create project
     * @param {*} req 
     * @param {*} res 
     */
    async store(req, res) {
        // const { filename } = req.file;
        req.body.date_start = moment(req.body.date_start).format("YYYY-MM-DDTHH:mm:ss");
        req.body.date_end = moment(req.body.date_end).format("YYYY-MM-DDTHH:mm:ss");

        let projectFineld = {
            ...req.body,
            category: req.body.category.split(',').map(category => category.trim()),
            'user.id_user': jwt.decode(req.headers.authorization).id,
            status: "create",
            // photo_project: filename
        }
        
        try {
            let projectFind = await projects.create(projectFineld)
            res.status(200).json({
                messageCode: 0,
                message: {
                    title: "Sucesso",
                    message: "Projeto Criado com sucesso!"
                },
                data: projectFind
            })
        } catch (error) {
            res.status(400).json({
                messageCode: 3,
                message: {
                    title: "Erro",
                    message: "Não foi possível criar projeto"
                },
                erro: error
            })
        }
    }

    /**
     * Update project
     * @param {*} req 
     * @param {*} res 
     */
    async update(req, res) {
        try {
            //const { filename } = req.file;
            req.body.date_start = moment(req.body.date_start).format("YYYY-MM-DDTHH:mm:ss");
            req.body.date_end = moment(req.body.date_end).format("YYYY-MM-DDTHH:mm:ss");

            let body = {
                ...req.body,
                category: req.body.category.split(',').map(category => category.trim()),
                //photo_project: filename
            }
            let projectUpdate = await projects.findByIdAndUpdate({ _id: req.params.id }, body, { new: true})
            projectUpdate 
            ?
                res.status(200).json({
                    messageCode: 0,
                    message: {
                        title: "Sucesso",
                        message: "Projeto atualizado com sucesso!"
                    },
                    data: projectUpdate
                })
            :
                res.status(400).json({
                    messageCode: 3,
                    message: {
                        title: "Erro",
                        message: "Não foi possível editar projeto"
                    },
                    error: {}
                })
        } catch (error) {
            res.status(400).json({
                messageCode: 3,
                message: {
                    title: "Erro",
                    message: "Não foi possível editar projeto"
                },
                error: error
            })
        }
    }

    /**
     * View project by Id
     * @param {*} req 
     * @param {*} res 
     */
    async showById(req, res) {
        try {
            let projetoId = await projects.findOne({ _id: req.params.id }).exec();
            res.status(200).json({
                messageCode: 0,
                message: {
                    title: "Sucesso",
                    message: "projeto listado com sucesso!"
                },
                data: projetoId
            })
        } catch (error) {
            res.status(400).json({
                messageCode: 3,
                message: {
                    title: "Erro",
                    message: "Erro no banco de dados"
                },
                erro: error
            })
        }
    }

    /**
     * List all project in user
     * @param {*} req 
     * @param {*} res 
     */
    async show(req, res) {
        try {
            let page = 1
            let results = 100
            let queryGeneric = {
                $or: [
                    {
                        'user.id_user': req.query.id_user
                    },{
                        'user.id_user_sharing': {
                            $elemMatch: {
                                $eq: req.query.id_user
                            }
                        }
                    }
                ]
            }

            if (req.query.page)
                page = parseInt(req.query.page)
            if (req.query.results)
                results = parseInt(req.query.results);

            projects.countDocuments(queryGeneric, async (err, count) => {
                projects.find({ $or: [
                    {
                        'user.id_user': req.query.id_user
                    },{
                        'user.id_user_sharing': {
                            $elemMatch: {
                                $eq: req.query.id_user
                            }
                        }
                    }
                ]}, (err, finded) => {
                    res.status(200).send({
                        messageCode: 0,
                        count: count,
                        pageNumbers: Math.ceil((count / results)),
                        page: page,
                        data: finded,
                        message: {
                            title: "Sucesso",
                            message: "Projetos listados com sucesso"
                        }
                    })
                })
                .limit(results)
                .skip((page * results) - results)
                .exec()
            })
        } catch (error) {
            res.status(400).json({
                messageCode: 3,
                message: {
                    title: "Erro",
                    message: "Não foi possível listar os projetos"
                },
                erro: error
            })
        }
    }

    /**
     * Share project with outher users
     * @param {*} req 
     * @param {*} res 
     */
    async share(req, res) {
        try {
            if(jwt.decode(req.headers.authorization).id != req.query.id_user){
                let projectoVerifica = await projects.findOne({ _id: req.query.id_project }).exec();
                if(projectoVerifica.user.id_user_sharing.indexOf(req.query.id_user) > -1) {
                    res.status(400).json({
                        messageCode: 3,
                        message: {
                            title: "Erro",
                            message: "Esse usuário já faz parte desse projeto"
                        }
                    })
                } else {
                    let projectUpdate = await projects.findOneAndUpdate({ _id: req.query.id_project }, {
                        $push: { 
                            'user.id_user_sharing': req.query.id_user
                        }
                    }, { new: true }).exec();
                    projectUpdate 
                    ? 
                        res.status(200).json({
                            messageCode: 0,
                            message: {
                                title: "Sucesso",
                                message: "Projeto compartilhado com sucesso"
                            },
                            data: projectUpdate
                        })
                    :
                        res.status(400).json({
                            messageCode: 3,
                            message: {
                                title: "Erro",
                                message: "Não foi possível compartilhar o projeto"
                            }
                        })
                }
            } else {
                res.status(401).json({
                    messageCode: 3,
                    message: {
                        title: "Erro",
                        message: "Não é possível compartilhar o projeto com você mesmo"
                    }
                })
            }
            
        } catch (error) {
            res.status(401).json({
                messageCode: 3,
                message: {
                    title: "Erro",
                    message: "Não foi possível compartilhar o projeto"
                }
            })
        }
    }

    /**
     * List users of project
     * @param {*} req 
     * @param {*} res 
     */
    async userProject(req, res) {
        try {
            let projetoUsers = await projects.findOne({ _id: req.query.id_project }, '-password');
            if(projetoUsers) {
                let usersProject = await users.aggregate([
                    {
                        $match: {
                            _id: { $in: projetoUsers.user.id_user_sharing }
                        }
                    }
                ])
                res.status(200).json({
                    messageCode: 0,
                    message: {
                        title: "Sucesso",
                        message: "Usuários listados com sucesso"
                    },
                    data: usersProject
                })
            } else {
                res.status(400).json({
                    messageCode: 3,
                    message: {
                        title: "Erro",
                        message: "Não foi possível listar os usuáriso do projeto"
                    }
                })
            }
        } catch (error) {
            res.status(400).json({
                messageCode: 3,
                message: {
                    title: "Erro",
                    message: "Não foi possível listar os usuáriso do projeto"
                }
            })
        }
    }

    /**
     * Start project
     * @param {*} req 
     * @param {*} res 
     */
    async startProject (req, res) {
        try {
            let projectStatus = await projects.findOne({ _id: req.params.id }).exec();
            if (projectStatus.status == "finalized")
                res.status(400).json({
                    messageCode: 3,
                    message: {
                        title: "Erro",
                        message: "Não foi possível inicar, pois o projeto estar finalizado"
                    }
                })
            
            let projectsUpdate = await projects.findOneAndUpdate({ _id: req.params.id }, {
                status: "running"
            }, { new: true }).exec();

            projectsUpdate 
            ? 
                res.status(200).json({
                    messageCode: 0,
                    message: {
                        title: "Sucesso",
                        message: "Projeto iniciado com sucesso"
                    },
                    data: projectsUpdate
                })
            :
                res.status(400).json({
                    messageCode: 3,
                    message: {
                        title: "Erro",
                        message: "Não foi possível inicar o projeto"
                    }
                })
        } catch (error) {
            res.status(401).json({
                messageCode: 3,
                message: {
                    title: "Erro",
                    message: "Não foi possível iniciar o projeto"
                }
            })
        }
    }

    /**
     * End project
     * @param {*} req 
     * @param {*} res 
     */
    async endProject (req, res) {
        try {
            let projectsUpdate = await projects.findOneAndUpdate({ _id: req.params.id }, {
                status: "finalized"
            }, { new: true }).exec();

            projectsUpdate 
            ? 
                res.status(200).json({
                    messageCode: 0,
                    message: {
                        title: "Sucesso",
                        message: "Projeto finalizado com sucesso"
                    },
                    data: projectsUpdate
                })
            :
                res.status(400).json({
                    messageCode: 3,
                    message: {
                        title: "Erro",
                        message: "Não foi possível finalizar o projeto, pois ainda não foi iniciado"
                    }
                })
        } catch (error) {
            res.status(401).json({
                messageCode: 3,
                message: {
                    title: "Erro",
                    message: "Não foi possível finalizar o projeto"
                }
            })
        }
    }

    /**
     * Cancel project
     * @param {*} req 
     * @param {*} res 
     */
    async cancelProject (req, res) {
        try {
            let projectFind = await projects.findOne({ _id: req.params.id }).exec();
            if(projectFind){
                if(projectFind.status === "finalized") {
                    res.status(200).json({
                        messageCode: 3,
                        message: {
                            title: "Erro",
                            message: "Projeto está finalizado, desse modo não ser cancelado"
                        }
                    })
                } else {
                    let projectsUpdate = await projects.findOneAndUpdate({ _id: req.params.id }, {
                        status: "canceled"
                    }, { new: true }).exec();

                    projectsUpdate 
                    ? 
                        res.status(200).json({
                            messageCode: 0,
                            message: {
                                title: "Sucesso",
                                message: "Projeto cancelado com sucesso"
                            },
                            data: projectsUpdate
                        })
                    :
                        res.status(400).json({
                            messageCode: 3,
                            message: {
                                title: "Erro",
                                message: "Não foi possível cancelar o projeto"
                            }
                        })
                }
            } else {
                res.status(401).json({
                    messageCode: 3,
                    message: {
                        title: "Erro",
                        message: "Não foi possível cancelar o projeto"
                    }
                })
            }
        } catch (error) {
            res.status(401).json({
                messageCode: 3,
                message: {
                    title: "Erro",
                    message: "Não foi possível cancelar o projeto"
                }
            })
        }
    }

    /**
     * Deleter project
     * @param {*} req 
     * @param {*} res 
     */
    async delete (req, res) {
        try {
            await projects.findOneAndDelete({ _id: req.params.id })
            res.status(200).json({
                messageCode: 0,
                message: {
                    title: "Sucesso",
                    message: "Projeto excluído com sucesso!"
                },
            })
        } catch (error) {
            res.status(400).json({
                messageCode: 3,
                message: {
                    title: "Erro",
                    message: "Não foi possível excluir projeto"
                },
                erro: error
            })
        }
    }

    async showAll(req, res) {
        try {
            let projeto = await projects.find(
                { $or: [
                    {
                        'user.id_user': new objectId(req.params.id)
                    },{
                        'user.id_user_sharing': {
                            $elemMatch: {
                                $eq: new objectId(req.params.id)
                            }
                        }
                    }
                ]}, { _id: 1 }
            );
            res.status(200).json({
                messageCode: 0,
                message: {
                    title: "Sucesso",
                    message: "Projetos Listados com sucesso!"
                },
                data: projeto
            })
        } catch (error) {
            res.status(400).json({
                messageCode: 3,
                message: {
                    title: "Erro",
                    message: "Não foi possível listar os projetos"
                },
                erro: error
            })
        }
    }

    // async showUserProject(req, res, next) {
    //     try {
    //         let projeto = await projects.aggregate([
    //             {
    //                 $match: {
    //                     $or: [
    //                         {
    //                             'user.id_user': req.query.id_user
    //                         },{
    //                             'user.id_user_sharing': {
    //                                 $elemMatch: {
    //                                     $eq: req.query.id_user
    //                                 }
    //                             }
    //                         }
    //                     ]
    //                 }, 
    //             }, {
    //                 $sort: {
    //                     _id: -1
    //                 }
    //             },
    //             {
    //                 $lookup: {
    //                     from: "users",
    //                     let: { 
    //                         id: "$user.id_user_sharing"
    //                     },
    //                     pipeline: [
    //                         {
    //                             $match: {
    //                                 $expr: {
    //                                     $and: [
    //                                         { $eq: ['$_id', '$$id'] }
    //                                     ]
    //                                 }
    //                             }
    //                         }
    //                     ],
    //                     as: "user"
    //                 },
    //             }, {
    //                 $unwind: {
    //                     path: "$user"
    //                 }
    //             }
    //         ]).exec();
    //         res.status(200).json({
    //             messageCode: 0,
    //             message: {
    //                 title: "Sucesso",
    //                 message: "Usuários do projeto listado com sucesso!"
    //             },
    //             data: projeto
    //         })
    //     } catch (error) {
    //         res.status(400).json({
    //             messageCode: 3,
    //             message: {
    //                 title: "Erro",
    //                 message: "Não foi possível listar os usuários"
    //             },
    //             erro: error
    //         })
    //     }
    // }
} 

module.exports = new Project();
